package sbu.cs;

import java.io.*;

public class FileManager {
    public void sendFile(SocketClass socket, FileMsg fileMsg) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(fileMsg.getFile());
        for (long i = 0; i < fileMsg.getNumOfPart(); i++) {
            if ((i == fileMsg.getNumOfPart() - 1) && (fileMsg.getExtraBytes() != 0)) {
                socket.sendMessage(fileInputStream.readNBytes(fileMsg.getExtraBytes()));
            } else {
                socket.sendMessage(fileInputStream.readNBytes(FileMsg.PART_SIZE));
            }
        }

        fileInputStream.close();
    }

    public void getFile(SocketClass socket, long parts, String fileName, String dir) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(dir + "/" + fileName);

        for (long i = 0; i < parts; i++) {
            byte[] byt = socket.readMessage();
            fileOutputStream.write(byt);
        }

        fileOutputStream.close();
    }
}
