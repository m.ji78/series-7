package sbu.cs;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

public class ClientClass {
    public SocketClass socket;

    public ClientClass(String address, int port) throws IOException {
        this.socket = new SocketClass(address, port);
    }

    public void sendMessage(String path) throws IOException {
        File file = new File(path);
        FileMsg fileMsg = new FileMsg(file);
        FileManager fileManager = new FileManager();

        byte[] numOfPart = longToBytes(fileMsg.getNumOfPart());
        socket.sendMessage(numOfPart);

        byte[] fileName = file.getName().getBytes();
        socket.sendMessage(fileName);

        fileManager.sendFile(this.socket, fileMsg);

        close();
    }

    public byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(x);
        return buffer.array();
    }

    public void close() throws IOException {
        socket.close();
    }

}
