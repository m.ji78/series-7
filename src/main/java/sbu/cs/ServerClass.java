package sbu.cs;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;

public class ServerClass {
    private SocketClass ss;
    private Socket socket;
    private final ServerSocket serverSocket;

    public ServerClass(int port) throws IOException {
        System.out.println("Trying to establish on port...");
        this.serverSocket = new ServerSocket(port);
        System.out.println("Server is established");
    }

    public void listen(String dir) throws IOException {
        System.out.println("waiting for a client connection...");
        socket = serverSocket.accept();
        System.out.println("client connected");

        this.ss = new SocketClass(socket);

        receiveMessage(dir);
    }

    public void receiveMessage(String dir) throws IOException {
        byte[] partBytes = ss.readMessage();
        ByteBuffer byteBuffer = ByteBuffer.allocate(8);

        byteBuffer.put(partBytes);
        byteBuffer.flip();
        long parts = byteBuffer.getLong();

        byte[] nameBytes = ss.readMessage();
        String name = new String(nameBytes);

        FileManager fileManager = new FileManager();
        fileManager.getFile(ss, parts, name, dir);

        //close();
    }

    public void close() throws IOException {
        serverSocket.close();
        socket.close();
        ss.close();
    }


}
