package sbu.cs;

import java.io.*;
import java.net.Socket;

public class SocketClass {
    public Socket socket;
    OutputStream out;
    InputStream in;

    public SocketClass(String host, int port) throws IOException {
        socket = new Socket(host, port);
        out = socket.getOutputStream();
        in = socket.getInputStream();
    }

    public SocketClass(Socket s) throws IOException {
        socket = s;
        out = socket.getOutputStream();
        in = socket.getInputStream();
    }

    public byte[] readMessage() throws IOException {
        DataInputStream dataInputStream = new DataInputStream(in);
        int length = dataInputStream.readInt();
        return dataInputStream.readNBytes(length);
    }

    public void sendMessage(byte[] message) throws IOException {
        DataOutputStream dataOutputStream = new DataOutputStream(out);
        dataOutputStream.writeInt(message.length);
        dataOutputStream.write(message);
    }

    public void close() throws IOException {
        socket.close();
    }
}

